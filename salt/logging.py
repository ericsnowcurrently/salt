'''salt.logging module

This is were Salt's logging get's setup.

:copyright: 2011 :email:`Pedro Algarvio (pedro@algarvio.me)`
:license: Apache 2.0, see LICENSE for more details.

'''

import logging
import logging.handlers
from contextlib import contextmanager


BASE_FORMAT = '[%(name)-15s][%(levelname)-8s] %(message)s'
CONSOLE_FORMAT = '%(asctime)s,%(msecs)03.0f ' + BASE_FORMAT
LOGFILE_FORMAT = '%(asctime)s ' + BASE_FORMAT
DATE_FORMAT = "%H:%M:%S"


# new log levels

TRACE = 5
GARBAGE = 1

LOG_LEVELS = {
    'debug': logging.DEBUG,
    'error': logging.ERROR,
    'garbage': GARBAGE,
    'info': logging.INFO,
    'none': logging.NOTSET,
    'trace': TRACE,
    'warning': logging.WARNING,
}


# functions for new log levels

def garbage(self, msg, *args, **kwargs):
    return self.log(GARBAGE, msg, *args, **kwargs)

def trace(self, msg, *args, **kwargs):
    return self.log(TRACE, msg, *args, **kwargs)

def inject_levels():
    Logger = logging.getLoggerClass()
    if not hasattr(Logger, "garbage"):
        Logger.garbage = garbage
        Logger.trace = trace
        logging.addLevelName(5, 'TRACE')
        logging.addLevelName(1, 'GARBAGE')


def set_logger_level(loggername, level):
    '''
    Tweak a specific logger's logging level.

    '''
    level = LOG_LEVELS.get(level.lower(), logging.ERROR)
    logging.getLogger(loggername).setLevel(level)


# helpers to set up logging

def configure_root_logger(handler, formatter, level):
    '''
    Add a handler to the root logger and add new logging levels.

    '''
    level = LOG_LEVELS.get(level.lower(), logging.ERROR)
    handler.setLevel(level)
    handler.setFormatter(formatter)

    inject_levels()
    root = logging.getLogger()
    root.setLevel(1) # lowest possible level
    root.addHandler(handler)


def setup_console_logger(level):
    '''
    Set up the console logger.

    '''
    handler = logging.StreamHandler()
    formatter = logging.Formatter(CONSOLE_FORMAT, DATE_FORMAT)
    configure_root_logger(handler, formatter, level)


def setup_logfile_logger(logpath, level):
    '''
    Setup the logfile logger.

    '''
    handler = logging.WatchedFileHandler(logpath, 'a', 'utf-8', delay=0)
    formatter = logging.Formatter(LOGFILE_FORMAT)
    configure_root_logger(handler, formatter, level)


def configure_logging(config):
    consolelevel = config("consoleloglevel")
    if consolelevel:
        setup_console_logger(consolelevel)

    setup_logfile_logger(
            config['log_file'],
            config['log_level'],
            )
    for name, level in config['log_granular_levels'].items():
        set_logger_level(name, level)

