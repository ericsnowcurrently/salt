'''
Process json with the jinja2 templating engine

This renderer will take a json file with the jinja template and render it to a
high data format for salt states.
'''

import json
import os
from salt.utils.jinja import get_template
from salt.renderers import get_context


def render(session, template_file):
    '''
    Render the data passing the functions and grains into the rendering system
    '''
    if not os.path.isfile(template_file):
        return {}

    context = get_context(session)
    template = get_template(session, template_file)

    json_data = template.render(**context)

    return json.loads(json_data)
