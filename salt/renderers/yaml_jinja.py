'''
The default rendering engine, process yaml with the jinja2 templating engine

This renderer will take a yaml file with the jinja2 template and render it to a
high data format for salt states.
'''

import os
import yaml
from salt.utils.jinja import get_template
from salt.renderers import get_context


def render(session, template_file):
    '''
    Render the data passing the functions and grains into the rendering system
    '''
    if not os.path.isfile(template_file):
        return {}

    context = get_context(session)
    template = get_template(session, template_file)

    yaml_data = template.render(**context)

    return yaml.safe_load(yaml_data)
