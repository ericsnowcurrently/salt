'''
Pure python state renderer

The sls file should contain a function called ``sls`` which returns high state
data
'''

import imp
import os

from salt.renderers import get_context


def render(session, template):
    '''
    Render the python module's components
    '''
    if not os.path.isfile(template):
        return {}

    mod = imp.load_source(
            os.path.basename(template).split('.')[0],
            template
            )
    context = get_context(session)

    return mod.run(context)
