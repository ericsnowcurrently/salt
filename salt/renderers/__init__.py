

class _Context(dict):
    def __getattr__(self, name):
        if name not in self:
            raise AttributeError(name)
        return self[name]


def _get_context(session, **names):
    if not names:
        names = session.opts.renderer_context
    context = _Context()
    for name, actualname in names.items():
        if actualname not in session:
            raise NameError(actualname)
        context[name] = session[actualname]
    return context

