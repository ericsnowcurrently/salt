'''
Process yaml with the Mako templating engine

This renderer will take a yaml file within a mako template and render it to a
high data format for salt states.
'''

import os

# Import Third Party libs
from mako.template import Template
import yaml

from salt.renderers import get_context


def render(session, template):
    '''
    Render the data passing the functions and grains into the rendering system
    '''
    if not os.path.isfile(template):
        return {}

    context = get_context(session)
    template = Template(open(template, 'r').read())
    yaml_data = template.render(**context)

    return yaml.safe_load(yaml_data)
