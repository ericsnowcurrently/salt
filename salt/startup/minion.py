'''
The management of salt command line utilities are stored in here

'''

# Import python libs
import optparse
import os
import sys
import yaml
import json

# Import salt components
import salt.cli.caller
import salt.cli.cp
import salt.cli.key
import salt.client
import salt.output
import salt.runner

from salt import __version__ as VERSION
from salt.exceptions import SaltInvocationError


OPTIONS = (OPTION_DAEMON, OPTION_CONFIG, OPTION_LOGLEVEL)


DEFAULTS = {
        'timeout': 5, # seconds
        'output': 'txt',
        'matcher': 'glob',
        'consoleloglevel': 'warning',
        }

DIR_CONFIGS = ['pki_dir', 'cachedir', 'log_file']


ENVVARS = {
        'SALT_MINION_CONFIG': CONFIG_NAME,
        }


def start(config):
    '''
    Start the salt minion.

    '''
    verify_env([self.opts['pki_dir'], self.opts['cachedir'],
            os.path.dirname(self.opts['log_file']),
            ])
    import salt.log
    salt.log.setup_logfile_logger(
        self.opts['log_file'], self.opts['log_level']
    )
    for name, level in self.opts['log_granular_levels'].iteritems():
        salt.log.set_logger_level(name, level)

    import logging

    # Late import so logging works correctly
    import salt.minion
    log = logging.getLogger(__name__)
    try:
        if self.cli['daemon']:
            # Late import so logging works correctly
            import salt.utils
            salt.utils.daemonize()
        minion = salt.minion.Minion(self.opts)
        minion.tune_in()
    except KeyboardInterrupt:
        log.warn('Stopping the Salt Minion')
        raise SystemExit('\nExiting on Ctrl-c')

