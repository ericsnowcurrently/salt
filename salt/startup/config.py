'''
All salt configuration loading and defaults should be in this module
'''

from __future__ import print_function

# Import python modules
import os
import os.path
import socket
import sys

# import third party libs
import yaml


CONFIG_NAME = "config"


def _load_configfile(path):
    config = {}
    if path and not os.path.isfile(path):
        print('Missing configuration file: {0}'.format(path))
    elif path:
        try:
            config = yaml.safe_load(open(path, 'r')) or config
        except Exception, e:
            msg = 'Error parsing configuration file: {0} - {1}'
            print(msg.format(path, e))
    return config


def load(defaults={}, envvars={}, overrides={}):
    '''
    Return a config built from the defaults, config file, and overrides.

    'envvars' is a mapping between environment variable names and config
    names.

    The path to the config file is looked for first in the overrides,
    then in an environment variable that maps to CONFIG_NAME (if any).
    Finally, it's looked for in the defaults.

    Precedence for config values follows this order:
    1. overrides
    2. shell environment variables
    3. config file
    4. defaults

    '''
    config = defaults.copy()

    # pull in the config file
    path = overrides.get(CONFIG_NAME)
    if not path or not os.path.isfile(path):
        envconfig = [k for k, v in envvars.items() if v == CONFIG_NAME]
        if envconfig:
            path = os.environ.get(envconfig[0])
    if not path or not os.path.isfile(path):
        path = defaults.get(CONFIG_NAME)
    config.update(_load_configfile(path))

    # pull in the environment variables
    for envvar in envvars:
        if envvar in os.environ:
            config[envvars[envvar]] = os.environ.get(envvar)

    # pull in the overrides
    config.update(overrides)

    # fix up the config
    if 'id' in config:
        # allow using numeric ids: convert int to string
        config['id'] = str(config['id'])
    config['conf_file'] = path

    return config


def prepend_root_dir(opts, path_options):
    '''
    Prepends the options that represent filesystem paths with value of the
    'root_dir' option.
    '''
    for path_option in path_options:
        if path_option in opts:
            opts[path_option] = os.path.normpath(
                    os.path.join([opts['root_dir'], opts[path_option]]))


def dns_check(addr):
    '''
    Return the validated IPv4 address, resolving first if passed a name.

    '''
    try:
        socket.inet_aton(addr)
        # is a valid ip addr
    except socket.error:
        # Not a valid ip addr, check if it is an available hostname
        try:
            addr = socket.gethostbyname(addr)
        except socket.gaierror:
            # Woah, this addr is totally bogus, die!!!
            err = ('The master address {0} could not be validated, please '
                   'check that the specified master in the minion config '
                   'file is correct\n')
            err = err.format(addr)
            print(err, file=sys.stderr)
            sys.exit(42)
    return addr


def ensure_directory(path):
    """
    Create the directory if missing.

    """
    if not os.path.isdir(path):
        try:
            os.makedirs(path)
        except OSError, e:
            msg = 'Failed to create directory path "{}" - {}'
            print(msg.format(path, e))


def validate(required_dirs):
    '''
    Ensure each directory exists.

    '''
    for dir_ in required_dirs:
        ensure_directory(dir_)

    [
            os.path.join(config['pki_dir'], 'minions'),
            os.path.join(config['pki_dir'], 'minions_pre'),
            os.path.join(config['cachedir'], 'jobs'),
            os.path.dirname(config['log_file']),
            config['sock_dir'],
            ]
