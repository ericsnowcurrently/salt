"""salt.cli package

"""

import os
import os.path
from optparse import OptionParser

from salt import __version__
from salt.config import load as load_config, validate as validate_config
from salt.logging import configure_logging, LOG_LEVELS



class CallArgs(namedtuple("CallArgs", "args kwargs")):
    def __new__(cls, *args, **kwargs):
        return super(cls, cls).__new__(cls, (args, kwargs))


OPTION_DAEMON = CallArgs(
        '-d', '--daemon',
        default=False,
        action='store_true',
        help='Run in a daemon',
        )

OPTION_CONFIG = CallArgs(
        '-c', '--config',
        help='Pass in an alternative configuration file',
        )

OPTION_LOGLEVEL = CallArgs(
        '-l', '--log-level',
        dest='consoleloglevel',
        choices=tuple(LOG_LEVELS),
        help=("Console log level. One of {}. For the logfile settings "
              "see the config file. Default: '%default'.").format(
              ', '.join(repr(l) for l in LOG_LEVELS)),
        )

OUTPUT_HELP = ""
OPTION_OUTPUT = CallArgs(
        '--output',
        choices=("txt", "raw", "yaml", "json"),
        help=OUTPUT_HELP,
        )

OPTION_TIMEOUT = CallArgs(
        '-t', '--timeout',
        type=int,
        help='Set the return timeout for batch jobs',
        )

MATCHER_HELP = ""
MATCHERS = ("glob", "pcre", "list", "grain", "exsel", "nodegroup", "compound")
OPTION_MATCHER = CallArgs(
        '-M', '--matcher',
        choices=MATCHERS,
        default="glob",
        help=MATCHER_HELP,
        ),


"""
        (('-E', '--pcre'), dict(
                default=False,
                dest='pcre',
                action='store_true',
                help=('Instead of using shell globs to evaluate the target '
                      'servers, use pcre regular expressions')),
        ),
        (('-L', '--list'), dict(
                default=False,
                dest='list_',
                action='store_true',
                help=('Instead of using shell globs to evaluate the target '
                      'servers, take a comma delimited list of servers.')),
        ),
        (('-G', '--grain'), dict(
                default=False,
                dest='grain',
                action='store_true',
                help=('Instead of using shell globs to evaluate the target '
                      'use a grain value to identify targets, the syntax '
                      'for the target is the grain key followed by a pcre '
                      'regular expression:\n"os:Arch.*"')),
        ),
        (('-X', '--exsel'), dict(
                default=False,
                dest='exsel',
                action='store_true',
                help=('Instead of using shell globs use the return code '
                      'of a function.'))
        ),
        (('-N', '--nodegroup'), dict(
                default=False,
                dest='nodegroup',
                action='store_true',
                help=('Instead of using shell globs to evaluate the target '
                      'use one of the predefined nodegroups to identify a '
                      'list of targets.'))
        ),
        (('-C', '--compound'), dict(
                default=False,
                dest='compound',
                action='store_true',
                help=('The compound target option allows for multiple '
                       'target types to be evaluated, allowing for greater '
                       'granularity in target matching. The compound target '
                       'is space delimited, targets other than globs are '
                       'preceted with an identifyer matching the specific '
                       'targets argument type: salt \'G@os:RedHat and '
                       'webser* or E@database.*\''))
        ),
"""

"""
        (('--raw-out',), dict(
                default=False,
                action='store_true',
                dest='raw_out',
                help=('Print the output from the salt command in raw python '
                      'form, this is suitable for re-reading the output into '
                      'an executing python script with eval.'))
        (('--text-out',), dict(
                default=False,
                action='store_true',
                dest='txt_out',
                help=('Print the output from the salt command in the same '
                      'form the shell would.'))
        (('--yaml-out',), dict(
                default=False,
                action='store_true',
                dest='yaml_out',
                help='Print the output from the salt command in yaml.')
        (('--json-out',), dict(
                default=False,
                action='store_true',
                dest='json_out',
                help='Print the output from the salt command in json.')
"""


def parse_cli(OPTIONS, USAGE=None):
    """Return the result of parsing the CLI.

    """
    parser = OptionParser(version="%prog {}".format(__version__),
                          usage=USAGE)
    for args, kwargs in OPTIONS:
        parser.add_option(*args, **kwargs)
    options, args = parser.parse_args()

    cli = {k: v for k, v in vars(options).items() if k != "help"}
    cli["args"] = args
    return cli
    

def load(module):
    '''
    Load the config for the module.

    The module should have the following attributes:
    - USAGE
    - OPTIONS
    - DEFAULTS
    - ENVVARS
    - finalize()

    '''
    if not hasattr(module, "USAGE"):
        module.USAGE = None
    overrides = parse_cli(module.USAGE, module.OPTIONS)
    config = load_config(module.DEFAULTS, module.ENVVARS, overrides)
    module.finalize(config)
    validate_config(config)
    configure_logging(config)

