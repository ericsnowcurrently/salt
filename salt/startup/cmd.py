'''
The management of salt command line utilities are stored in here

'''

from salt.startup.config import (OPTION_DAEMON, OPTION_CONFIG, OPTION_LOGLEVEL,
                                 CONFIG_NAME, prepend_root_dir)

from . import master
from .cli import CallArgs



# Import python libs
import optparse
import os
import sys
import yaml
import json

# Import salt components
import salt.cli.caller
import salt.cli.cp
import salt.cli.key
import salt.client
import salt.output
import salt.runner

from salt import __version__ as VERSION
from salt.exceptions import SaltInvocationError


OPTIONS = (
        OPTION_TIMEOUT,
        OPTION_MATCHERS,
        CallArgs(
                '--return',
                dest='returnmethod',
                help=('Set an alternative return method. By default salt will '
                      'send the return data from the command back to the '
                      'master, but the return data can be redirected into '
                      'any number of systems, databases or applications.'),
                ),
        CallArgs(
                '-Q', '--query',
                default=False,
                action='store_true',
                help=('Execute a salt command query, this can be used to find '
                      'the results os a previous function call: -Q test.echo'),
                ),
        OPTION_CONFIG,
        OPTION_OUTPUT,
        )


DEFAULTS = master.DEFAULTS


ENVVARS = {}


def finalize(config):

        #opts = {}
        #opts['timeout'] = options.timeout
        #opts['pcre'] = options.pcre
        #opts['list'] = options.list_
        #opts['grain'] = options.grain
        #opts['exsel'] = options.exsel
        #opts['nodegroup'] = options.nodegroup
        #opts['compound'] = options.compound
        #opts['return'] = options.return_
        #opts['conf_file'] = options.conf_file
        #opts['raw_out'] = options.raw_out
        #opts['txt_out'] = options.txt_out
        #opts['yaml_out'] = options.yaml_out
        #opts['json_out'] = options.json_out

    args = config.get("args", [])

    if config.get("query"):
        if not args:
            msg = 'Please pass in a command to query the old salt calls for.'
            print(msg, file=sys.stderr)
            sys.exit('2')
        config["cmd"] = args[0]
    else:
        # Catch invalid invocations of salt such as: salt run
        if len(args) <= 1:
            parser.print_help()
            parser.exit()

        if opts['list']:
            opts['tgt'] = args[0].split(',')
        else:
            opts['tgt'] = args[0]

        if ',' in args[1]:
            opts['fun'] = args[1].split(',')
            opts['arg'] = []
            for comp in ' '.join(args[2:]).split(','):
                opts['arg'].append(comp.split())
            if len(opts['fun']) != len(opts['arg']):
                err = ('Cannot execute compound command without defining '
                       'all arguments.')
                sys.stderr.write(err + '\n')
                sys.exit(42)
        else:
            opts['fun'] = args[1]
            opts['arg'] = args[2:]

