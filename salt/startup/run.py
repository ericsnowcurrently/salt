'''
The management of salt command line utilities are stored in here

'''

# Import python libs
import optparse
import os
import sys
import yaml
import json

# Import salt components
import salt.cli.caller
import salt.cli.cp
import salt.cli.key
import salt.client
import salt.output
import salt.runner

from salt import __version__ as VERSION
from salt.exceptions import SaltInvocationError


OPTIONS = (OPTION_CONFIG, OPTION_DOC)
DEFAULTS = master.DEFAULTS
ENVVARS = master.ENVVARS

        """
        parser.add_option('-d',
                '--doc',
                '--documentation',
                dest='doc',
                default=False,
                action='store_true',
                help=('Display documentation for runners, pass a module or '
                      'a runner to see documentation on only that '
                      'module/runner.'))
        """

def finalize(config):
    raise NotImplementedError


    opts['config'] = options.config
    opts['doc'] = options.doc

    if len(args) > 0:
        opts['fun'] = args[0]
    else:
        opts['fun'] = ''
    if len(args) > 1:
        opts['arg'] = args[1:]
    else:
        opts['arg'] = []

    opts.update(salt.config.master_config(options.config))






def run_runner(session):
    '''
    Execute the salt call!
    '''
    salt.runner.run(session)

