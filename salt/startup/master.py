'''
All startup code related to the salt master is in this module.

'''

from salt.startup.config import (OPTION_DAEMON, OPTION_CONFIG, OPTION_LOGLEVEL,
                                 CONFIG_NAME, prepend_root_dir)


OPTIONS = (OPTION_DAEMON, OPTION_CONFIG, OPTION_LOGLEVEL)

DEFAULTS = {
        'config': '/etc/salt/master',
        'interface': '0.0.0.0',
        'publish_port': '4505',
        'worker_threads': 5,
        'sock_dir': '/tmp/.salt-unix',
        'ret_port': '4506',
        'keep_jobs': 24,
        'root_dir': '/',
        'pki_dir': '/etc/salt/pki',
        'cachedir': '/var/cache/salt',
        'file_roots': {
                'base': ['/srv/salt'],
                },
        'file_buffer_size': 1048576,
        'hash_type': 'md5',
        #'conf_file': '/etc/salt/master',
        'open_mode': False,
        'auto_accept': False,
        'renderer': 'yaml_jinja',
        'failhard': False,
        'state_top': 'top.sls',
        'order_masters': False,
        'log_file': '/var/log/salt/master',
        'log_level': 'warning',
        'log_granular_levels': {},
        'cluster_masters': [],
        'cluster_mode': 'paranoid',
        'serial': 'msgpack',
        'nodegroups': {},
        'timeout': 5, # seconds
        'output': 'txt',
        'matcher': 'glob',
        'consoleloglevel': 'warning',
        }
DIR_CONFIGS = ['pki_dir', 'cachedir', 'log_file', 'sock_dir']

ENVVARS = {
        'SALT_MASTER_CONFIG': CONFIG_NAME,
        }


def finalize(config):
    '''
    Apply final adjustments to the config.

    '''

    # Prepend root_dir to other paths
    prepend_root_dir(config, DIR_CONFIGS)

    # Enabling open mode requires that the value be set to True, and nothing
    # else!
    if config.get("open_mode") is not True:
        config["open_mode"] = False
    if config.get("auto_accept") is not True:
        config["auto_accept"] = False

